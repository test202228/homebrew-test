class Localizegen < Formula
	desc "Generate Localization tools for Mobile Application an Android, iOS"
	homepage "https://gitlab.com/test202228/localizegen-test"
	url "https://gitlab.com/test202228/localizegen-test/-/archive/master/localizegen-test-master.tar.gz"
	sha256 "c8f3b1e3d5b73437e50657df12f6b68c966ea9a9ef056f58ad984ccf64d191bc"
	license "MIT"
	version '1.0.0'

	def install
		bin.install "localizegen"
	end

	test do
		system "#{bin}/localizegen"
	end
end
